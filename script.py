from flask import *  
   
app = Flask(__name__)  
  
@app.route('/')  
def message():  
    return "<html><body><h1>Hi, welcome to the website</h1></body></html>"  

@app.route('/admin')  
def admin():  
    return 'admin'  
  
@app.route('/librarion')  
def librarion():  
    return 'librarion'  
  
@app.route('/student')  
def student():  
    return 'student'  
  
@app.route('/user/<name>')  
def user(name):  
    if name == 'admin':  
        return redirect(url_for('admin'))  
    if name == 'librarion':  
        return redirect(url_for('librarion'))  
    if name == 'student':  
        return redirect(url_for('student'))  
	
if __name__ =='__main__':  
    app.run(debug = True)
